#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define N 500

typedef struct {
    int stackone[N];
    int stacktwo[N];
    int topone;
    int toptwo;
} CQueue;


CQueue* cQueueCreate() {//创建双栈实现队列先进先出
    CQueue *pCQueue = (CQueue*)calloc(1, sizeof(CQueue));//申请空间并初始化
    pCQueue->topone = -1;
    pCQueue->toptwo = -1;
    return pCQueue;
}

void cQueueAppendTail(CQueue* pcqueue, int value) {
    //入队先进入第一个栈中
    pcqueue->stackone[++(pcqueue->topone)] = value;
}

int cQueueDeleteHead(CQueue* pcqueue) {
    //出队按各栈的情况
    //两个栈为空
    if(pcqueue->topone == -1 && pcqueue->toptwo == -1){
        return -1;
    }
    //只有第二个栈为空，因此需要将第一个栈的（除最底的）出栈进入第二个栈中，输出最底的
    else if(pcqueue->toptwo == -1){
        while(pcqueue->topone > 0){
            pcqueue->stacktwo[++(pcqueue->toptwo)] = pcqueue->stackone[(pcqueue->topone)--];
        }
        return pcqueue->stackone[(pcqueue->topone)--];//返回第一个栈的最底的元素，即为队列首
    }
    //两边都有或只有第二个栈有，返回第二个栈的栈顶元素
    else{
       return pcqueue->stacktwo[(pcqueue->toptwo)--];
    }
}

void cQueueFree(CQueue* pcqueue) {
    free(pcqueue);
    pcqueue = NULL;
}

/**
 * Your CQueue struct will be instantiated and called as such:
 * CQueue* obj = cQueueCreate();
 * cQueueAppendTail(obj, value);
 
 * int param_2 = cQueueDeleteHead(obj);
 
 * cQueueFree(obj);
*/
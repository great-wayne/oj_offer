#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.h"

int main() {
	pListNode_t pHead = NULL;
	pListNode_t pTail = NULL;
	int data;
	while (scanf("%d", &data) != EOF) {
		sortInsert(&pHead, &pTail, data);
	}
	printList(pHead);
	unsigned int k;
	printf("input k :");
	scanf("%d", &k);
	if (FindKthToTail(pHead, k) != NULL) {
		printf("The %uth to tail is %d\n", k, FindKthToTail(pHead, k)->data);
	}
	system("pause");
	return 0;
}

ListNode_t* FindKthToTail(ListNode_t* pListHead, unsigned int k) {
	if (pListHead == NULL) {//�п�
		fprintf(stderr, "List is null\n");
		return NULL;
	}
	int nodeCount = 0;
	pListNode_t pCur = pListHead;
	while (pCur) {
		nodeCount += 1;
		pCur = pCur->pNext;
	}//������������
	if (k == 0) {
		fprintf(stderr, "K can't be zero\n");
		return NULL;
	 }
	else if (k > nodeCount) {
		fprintf(stderr, "K is more than list node\n");
		return NULL;
	}
	else {
		pCur = pListHead;
		pListNode_t pPre = pCur;
		for (unsigned int i = 0; i < k - 1; ++i) {
			pCur = pCur->pNext;
		}
		while (pCur->pNext) {
			pCur = pCur->pNext;
			pPre = pPre->pNext;
		}
		//printf("The %dth to Tail is %d\n", k, pPre->data);
		return pPre;
	}
}

void sortInsert(pListNode_t *pplisthead, pListNode_t *pplisttail, int data) {
	pListNode_t pNew = (pListNode_t)calloc(1, sizeof(ListNode_t));
	pNew->data = data;
	if (*pplisthead == NULL) {
		*pplisthead = pNew;
		*pplisttail = pNew;
	}
	else if (data < (*pplisthead)->data) {
		pNew->pNext = *pplisthead;
		*pplisthead = pNew;
	}
	else {
		pListNode_t pPre = *pplisthead;
		pListNode_t pCur = pPre->pNext;
		while (pCur) {
			if (pCur->data > data) {
				pNew->pNext = pCur;
				pPre->pNext = pNew;
				break;
			}
			pPre = pCur;
			pCur = pCur->pNext;
		}
		if (pCur == NULL) {
			(*pplisttail)->pNext = pNew;
			*pplisttail = pNew;
		}
	}
}

void printList(pListNode_t plisthead) {
	pListNode_t pCur = plisthead;
	while (pCur) {
		printf("%3d", pCur->data);
		pCur = pCur->pNext;
	}
	puts("");
}
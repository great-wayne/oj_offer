typedef struct ListNode_s {
	int data;
	struct ListNode_s *pNext;
}ListNode_t,*pListNode_t;
ListNode_t* FindKthToTail(ListNode_t* pListHead, unsigned int k);
void sortInsert(pListNode_t *pplisthead, pListNode_t *pplisttail, int data);
void printList(pListNode_t plisthead);
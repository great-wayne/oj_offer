/*
*  File    minArray.cc
*  Author  WayneGreat
*  Date    2021-09-15  09:13:09
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::cerr;

class Solution
{
public:
    int minArray(vector<int> &numbers)
    {
        if (numbers.empty()) cerr << "numbers is empty" << endl;

        int index1 = 0;
        int index2 = numbers.size() - 1;
        int indexMid = index1;

        while (numbers[index1] >= numbers[index2]) {
            if ((index2 - index1) == 1) {//当前后两个指针相邻时，index2所指为最小值
                indexMid = index2;
                break;
            }
            indexMid = (index2 + index1) / 2;//中点
            //  indexMid = (index2 + index1) >> 1;//OK
            if (numbers[indexMid] == numbers[index1] 
                && numbers[indexMid] == numbers[index2]) {
                //当前后中三个数相同时无法比较，需要从头遍历找最小值
                return minInorder(numbers);
                }
            if (numbers[indexMid] >= numbers[index1]) {
                index1 = indexMid;
            }
            if (numbers[indexMid] <= numbers[index2]) {
                index2 = indexMid;
            }
        }
        return numbers[indexMid];
    }

private:
    int minInorder(vector<int> &nums)
    {
        int ret = nums[0];
        for (int idx = 1; idx < nums.size() - 1; ++idx) {
            if (nums[idx] < ret) ret = nums[idx];
        }
        return ret;
    }
};
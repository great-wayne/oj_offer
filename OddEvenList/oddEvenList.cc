/*
*  File    oddEvenList.cc
*  Author  WayneGreat
*  Date    2021-09-09  20:13:32
*  Describe 
*/

#include <iostream>

using std::cout;
using std::endl;

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    ListNode *oddEvenList(ListNode *head)
    {
        if (head == nullptr) {
            return head;
        }

        ListNode *p = head;
        ListNode *q = head->next;
        ListNode *evenHead = q;//保存偶数节点头位置
        while (q != nullptr && q->next != nullptr) {
            p->next = p->next->next;
            p = p->next;
            q->next = q->next->next;
            q = q->next;
        }

        p->next = evenHead;

        return head;
    }
};
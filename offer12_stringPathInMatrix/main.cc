/*
题目：请设计一个函数，用来判断在一个矩阵中是否存在一条包含某字符串所有字符的路径。
路径可以从矩阵中任意一格开始，每一步可以在矩阵中向左、右、上、下移动一格。
如果一条路径经过了矩阵的某一格，那么该路径不能再次进入该格子。
例如在下面的3×4的矩阵中包含一条字符串“bfce”的路径（路径中的字母用下划线标出）。
但矩阵中不包含字符串“abfb”的路径，因为字符串的第一个字符b占据了矩阵中的第一行第二个格子之后，路径不能再次进入这个格子。
*/

//回溯算法->解决在二维数组中找路径的问题，路径匹配

#include <iostream>
#include <vector>
#include <string>
#include <string.h>

using std::vector;
using std::cout;
using std::endl;
using std::string;

class Solution {
public:
    bool exist(vector<vector<char>> &board, string word)
    {
        if(board.empty() || word.empty())
        {
            return false;
        }

        //获取board矩阵的行与列
        cols = board[0].size();
        rows = board.size();
        // cout << "cols = " << cols << endl;
        // cout << "rows = " << rows << endl;

        //设置对应矩阵位置的访问bool值
        bool *visited = new bool[cols * rows];
        // cout << "cols * rows = " << cols * rows << endl;
        memset(visited, 0, cols * rows);

        int pathLength = 0;
        //用于记录word的已找到字符路径的下标位置
        //当word[pathLength] == '\0' 时，路径字符串上的所有字符都在矩阵中找到合适的位置
        for(int row = 0; row < rows; ++row)
        //遍历矩阵的每个元素作为第一个路径，当有路径符合时就提前退出
        {
            for(int col = 0; col < cols; ++col)
            {
                if(hasPath(board, rows, cols, row, col, word, pathLength, visited))
                //进行检查
                {
                    return true;
                }
            }
        }

        delete [] visited;

        return false;
    }

private:
    bool hasPath(vector<vector<char>> &board,
                 int rows, int cols,
                 int row, int col, string &word,
                 int &pathLength, bool *visited)
    {
        if (word[pathLength] == '\0')
        {
            return true;
        }

        bool has_path = false;
        if(row >= 0 && row < rows &&  col >= 0 && col < cols
                    && board[row][col] == word[pathLength]
                    && !visited[row * cols + col])
        //矩阵元素与字符串中的位置一致，且还未被访问过
        {
            ++pathLength;//进入此格子，将路径长度加一
            visited[row * cols + col] = true;//访问标记为true

            //继续访问此格子的上下左右四个点，当四个都不能匹配时，退出此格子，返回上一个路径，回溯
            has_path = hasPath(board, rows, cols, row - 1, col,
                                word, pathLength, visited)
                    || hasPath(board, rows, cols, row, col - 1,
                                word, pathLength, visited)
                    || hasPath(board, rows, cols, row + 1, col,
                                word, pathLength, visited)
                    || hasPath(board, rows, cols, row, col + 1,
                                word, pathLength, visited);
            
            if(!has_path)
            {
                --pathLength;
                visited[row * cols + col] = false;
            }
        }

        return has_path;
    }

private:
    int rows;
    int cols;
};


void test()
{
    Solution sl;
    Solution sl2;
    vector<vector<char>> board =
    {
        {'A', 'B', 'C', 'E'},
        {'S', 'F', 'C', 'S'},
        {'A', 'D', 'E', 'E'}
    };

    string word = "ABCCED";
    string word2 = "ABE";

    bool ans = sl.exist(board, word);
    cout << "ans = " << ans << endl;
    bool ans2 = sl2.exist(board, word2);
    cout << "ans2 = " << ans2 << endl;
}

int main() 
{
    test();
    return 0;
}








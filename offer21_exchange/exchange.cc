/*
*  File    exchange.cc
*  Author  WayneGreat
*  Date    2021-09-15  12:38:36
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::cerr;

class Solution {
public:
    vector<int> exchange(vector<int>& nums) 
    {
        if(nums.empty())
        {
            cerr << "vector is empty. " << endl;
            return nums;
        }

        auto iterBegin = nums.begin();
        auto iterEnd = nums.end() - 1;
        
        while(iterBegin < iterEnd)
        {
            //将迭代器向后移动，直到它指向偶数
            //2 => 0010 & 0x1 => 0001 ===> 0000 代表为偶数，反之为奇数  
            while(iterBegin < iterEnd && (*iterBegin & 0x1) != 0)
            {
                ++iterBegin;
            }

            //将迭代器向前移动，直到它指向奇数
            while(iterBegin < iterEnd && (*iterEnd & 0x1) == 0)
            {
                --iterEnd;
            }

            //交换位置
            if(iterBegin < iterEnd)
            {
                int temp = *iterBegin;
                *iterBegin = *iterEnd;
                *iterEnd = temp;
            }
        }

        return nums;
    }
};
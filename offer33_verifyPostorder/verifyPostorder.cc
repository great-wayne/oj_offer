/*
*  File    verifyPostorder.cc
*  Author  WayneGreat
*  Date    2021-09-13  11:31:35
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

class Solution
{
public:
    bool verifyPostorder(vector<int> &postorder)//传入二叉搜索树的后续遍历序列
    {
        //不需要判空，应为leetcode中[]空数组也返回true
        // if (postorder.empty()) return false;//判空

        if (postorder.size() == 1) return true;//当二叉搜索树只有一个节点时，返回true
        return verifyPostorderCore(postorder, 0, postorder.size() - 1);
    }
private:
    bool verifyPostorderCore(vector<int> &postorder, int start, int end)
    {
        if (start >= end) return true;
        //剩余一个节点时，说明之前的的序列已经正确（没有返回false）
        //不需要再判断此节点下的序列是否符合二叉搜索树（因为已经没有节点）
        int low = start;
        while (low < end && postorder[low] < postorder[end]) {
            ++low;//找到左右子树的分割下标，即第一个大于end下标元素的值
        }
        for (int idx = low; idx < end; ++idx) {//判断右子树是否符合二叉搜索树
            if (postorder[idx] < postorder[end]) return false;
        }
        //继续递归左右子树是否 都(&&) 符合二叉搜索树
        return verifyPostorderCore(postorder, start, low - 1) &&
               verifyPostorderCore(postorder, low, end - 1);
    }
};
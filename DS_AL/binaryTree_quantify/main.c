//#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"

int main()
{
	pTreeNode_t pRoot = NULL; //二叉树的根结点
	//使用辅助队列进行不确定数量的层次建树
	pQueueNode_t pFront = NULL; //队列头指针
	pQueueNode_t pRear = NULL;	//队列尾指针
	char ch;
	while (scanf("%c", &ch) != EOF)
	{
		if (ch == '\n')
		{
			break;
		}
		buildBinaryTree(&pRoot, &pFront, &pRear, ch);
	}
	preOrder(pRoot); //先序遍历
	printf("\n");
	inOrder(pRoot); //中序遍历
	printf("\n");
	postOrder(pRoot); //后序遍历
	printf("\n");
	system("pause");
}
void buildBinaryTree(pTreeNode_t *ppRoot, pQueueNode_t *ppFront, pQueueNode_t *ppRear, char ch)
{
	pTreeNode_t pTreeNode = (pTreeNode_t)calloc(1, sizeof(TreeNode_t));
	pTreeNode->val = ch; //为树结点申请空间，并初始化val
	pQueueNode_t pQueueNode = (pQueueNode_t)calloc(1, sizeof(QueueNode_t));
	pQueueNode->treeNode = pTreeNode; //为队列结点申请空间，并初始化treeNOde指针指向树结点的地址
	if (*ppRoot == NULL)
	{						 //插入第一个结点
		*ppRoot = pTreeNode; //将第一个结点插入二叉树
		*ppFront = pQueueNode;
		*ppRear = pQueueNode; //入队
	}
	else
	{ //插入后续结点
		(*ppRear)->pNext = pQueueNode;
		*ppRear = pQueueNode;			   //尾插法入队
		pQueueNode_t pQueueCur = *ppFront; //pQueueNode指向可能待出队的结点，用于后续的free操作
		if (pQueueCur->treeNode->pLeft == NULL)
		{ //插入到左孩子
			pQueueCur->treeNode->pLeft = pTreeNode;
		}
		else
		{ //插入到右孩子
			pQueueCur->treeNode->pRight = pTreeNode;
			*ppFront = pQueueCur->pNext; //第一个队列结点出队
			free(pQueueCur);
			pQueueCur = NULL;
		}
	}
}

//先序遍历的递归实现
void preOrder(pTreeNode_t root)
{
	if (root)
	{
		printf("%c", root->val);
		preOrder(root->pLeft);
		preOrder(root->pRight);
	}
}

//中序遍历的递归实现
void inOrder(pTreeNode_t root)
{
	if (root)
	{
		inOrder(root->pLeft);
		printf("%c", root->val);
		inOrder(root->pRight);
	}
}

//后序遍历的递归实现
void postOrder(pTreeNode_t root)
{
	if (root)
	{
		postOrder(root->pLeft);
		postOrder(root->pRight);
		printf("%c", root->val);
	}
}

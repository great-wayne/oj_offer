typedef struct treeNode_s {//树结点
	char val;
	struct treeNode_s *pLeft;
	struct treeNode_s *pRight;
} TreeNode_t, *pTreeNode_t;
typedef struct queueNode_s {//队列结点
	pTreeNode_t treeNode;
	struct queueNode_s *pNext;
} QueueNode_t, *pQueueNode_t;
void preOrder(pTreeNode_t root);
void inOrder(pTreeNode_t root);
void postOrder(pTreeNode_t root);
void buildBinaryTree(pTreeNode_t *ppRoot, pQueueNode_t *ppFront, pQueueNode_t *ppRear, char ch);

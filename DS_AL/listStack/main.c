#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "stack.h"

int main()
{
    Stack_t stack;
    init(&stack);
    push(&stack, 2);
    push(&stack, 4);
    push(&stack, 1);
    push(&stack, 3);
    pop(&stack);
    printf("%d\n",top(&stack));
    pop(&stack);
    printf("%d\n",top(&stack));
    push(&stack, 0);
    printf("%d\n",top(&stack));
    system("pause");
    return 0;
}
void init(pStack_t pstack)
{
    memset(pstack, 0, sizeof(Stack_t));
}
int isEmpty(pStack_t pstack)
{
    if (pstack->Size == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
void push(pStack_t pstack, int data)
{
    pNode_t pNew = (pNode_t)calloc(1, sizeof(Node_t));
    pNew->data = data; //申请结点空间，赋值
    pNew->pNext = pstack->pHead;
    pstack->pHead = pNew; //入栈
    ++(pstack->Size);
}
void pop(pStack_t pstack)
{
    if (isEmpty(pstack))
    {
        fprintf(stderr, "stack is empty\n");
    }
    else
    { //出队
        pNode_t pCur = pstack->pHead;
        pstack->pHead = pCur->pNext;
        free(pCur);
        pCur = NULL;
        --(pstack->Size);
    }
}
int top(pStack_t pstack)
{
    if (isEmpty(pstack))
    {
        fprintf(stderr, "stack is empty\n");
        return -1;
    }
    else
    {
        return pstack->pHead->data;
    }
}
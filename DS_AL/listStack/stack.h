//链式栈
typedef struct Node_s {
    int data;
    struct Node_s *pNext;
} Node_t, *pNode_t;
typedef struct Stack_s {
    int Size;
    pNode_t pHead;
} Stack_t, *pStack_t;
void init(pStack_t pstack);//初始化
void push(pStack_t pstack, int data);//入栈
void pop(pStack_t pstack);//出栈
int isEmpty(pStack_t pstack);//判空
int top(pStack_t pstack);//获取头元素

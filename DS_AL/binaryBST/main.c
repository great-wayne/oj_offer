#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bst.h"

int main() {

	system("pause");
	return 0;
}

int bstSearch(pBSTNode_t pbstroot, int data) {
	pBSTNode_t pCur = pbstroot;
	if (pCur == NULL) {
		fprintf(stderr, "Tree is null\n");
		return -1;
	}
	while (pCur) {
		if (pCur->data < data) {
			pCur = pCur->pRight;
		}
		else if (pCur->data > data) {
			pCur = pCur->pLeft;
		}
		else {
			return 1;//1为查找成功
		}
	}
	return 0;//0为查找失败
}

void bstInsert(pBSTNode_t pbstroot, int data) {
	int flag;//判断插入是左还是右
	pBSTNode_t pNew = (pBSTNode_t)calloc(1, sizeof(BSTNode_t));
	pNew->data = data;
	pBSTNode_t pCur = pbstroot;//查找
	pBSTNode_t pPre = pbstroot;//改变指针域
	if (pCur == NULL) {//树为空
		fprintf(stderr, "Tree is null\n");
		return -1;
	}
	//检查根结点
	if (pCur->data < data) {
		pCur = pCur->pRight;
	}
	else if (pCur->data > data) {
		pCur = pCur->pLeft;
	}
	else {
		fprintf(stderr, "insert false\n");
		return;
	}

	while (pCur) {
		if (pCur->data < data) {
			pPre = pCur;
			pCur = pCur->pRight;
			flag = 0;//0插入右
		}
		else if (pCur->data > data) {
			pPre = pCur;
			pCur = pCur->pLeft;
			flag = 1;//1插入左
		}
		else {
			fprintf(stderr, "insert false\n");
			return;
		}
	}
	if (flag) {//1插入左
		pPre->pLeft = pNew;
		pNew->pParent = pPre;
	}
	else {//0插入右
		pPre->pRight = pNew;
		pNew->pParent = pPre;
	}
}
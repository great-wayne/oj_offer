typedef struct BSTNode_s {
	int data;
	struct BSTNode_s* pParent;
	struct BSTNode_s* pLeft;
	struct BSTNode_s* pRight;
} BSTNode_t, *pBSTNode_t;
int bstSearch(pBSTNode_t pbstroot, int data);
void bstInsert(pBSTNode_t pbstroot, int data);
/*
*  File    bubble.cc
*  Author  WayneGreat
*  Date    2021-08-26  17:09:54
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::swap;

#if 0
//上述 swap在stl_algobase.h中的实现，使用模板实现swap 
template <typename T>
inline void
swap(T &a, T &b)
{
    T tmp = a;
    a = b;
    b = tmp;
}
#endif

//时间复杂度 O(n^2) ，空间 O(1)，原地算法，稳定
void BubbleSort(vector<int>& nums)
{
    int length = nums.size();//获取数组长度

    // for(int idx = 0; idx < length; ++idx)//对比趟数，len趟，也可以但没必要
    for(int idx = 0; idx < length - 1; ++idx)//对比趟数，len-1趟，更符合规范理解
    {
        for(int j = 0; j < length - 1 - idx; ++j)//每趟进行两两对比
        {
            if(nums[j] > nums[j + 1])
            {
                swap(nums[j], nums[j + 1]);
            }
        }
    }
}

//优化版本，若已经有序，则不需要继续进行
void BubbleSort2(vector<int>& nums)
{
    int len = nums.size();
    bool flag = false;//false表示在一趟中没有进行交换

    for(int idx = 0; idx < len - 1; ++idx)
    {
        flag = false;//每趟初始设置为false

        for(int j = 0; j < len - 1 - idx; ++j)
        {
            if(nums[j] > nums[j + 1])
            {
                swap(nums[j], nums[j + 1]);
                flag = true;//表示有交换
            }
        }

        if(!flag)
        //一趟中没有元素交换，提前退出
        {
            break;
        }
    }
}

void test()
{
    vector<int> nums = {1, 6, 3, 4, 2, 5, 9, 7, 0, 8};

    // BubbleSort(nums);
    BubbleSort2(nums);

    for(auto &elem : nums)
    {
        cout << elem << " ";
    }
    cout << endl;
}

int main()
{
    test();
    return 0;
}

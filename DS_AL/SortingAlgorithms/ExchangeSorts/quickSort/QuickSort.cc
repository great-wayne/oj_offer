/*
*  File    QuickSort.cc
*  Author  WayneGreat
*  Date    2021-08-27  19:58:20
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::swap;

int Partition(vector<int>& nums, int start, int end)
{
    int endPos = start - 1;//定义一开始中轴元素的插入位置

    for (int idx = start; idx < end; ++idx) {
        if (nums[idx] < nums[end]) {
            //小于中轴元素的值就++endPos
            ++endPos;
            //此时endPos指向待交换的元素，左边全部的值小于中轴元素
            if(idx != endPos) {
            //不相等的是因为++idx过程中有大于end的值而endPos没有++
            //然后出现了小于end的值在endPos右方
                swap(nums[idx], nums[endPos]);
                //交换双方的值，endPos指向大于end的值，idx指向小于end的值
            }
        }
    }
    ++endPos;//endPos指向待插入的位置
    swap(nums[endPos], nums[end]);

    return endPos;//返回中轴元素的下标，用于进一步的递归
}

void QuickSort(vector<int>& nums, int start, int end)
{
    if (start == end) {//递归退出条件！！！
        return;
    }

    int index = Partition(nums, start, end);//获取中轴元素下标

    //分别递归排序中轴元素左右
    if (index > start) {
        QuickSort(nums, start, index - 1);
    }
    if (index < end) {
        QuickSort(nums, index + 1, end);
    }
}

void test()
{
    vector<int> nums = {1, 5, 3, 4, 6, 2, 4, 7, 9, 0};

    QuickSort(nums, 0, nums.size() - 1);

    for (auto& elem : nums) {
        cout << elem << " ";
    }
    cout << endl;
}

int main()
{
    test();
    return 0;
}

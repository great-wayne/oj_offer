/*
*  File    MergeSort.cc
*  Author  WayneGreat
*  Date    2021-08-28  16:51:45
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void MergeSort(vector<int>& nums, vector<int>& tmp, int start, int end)
//需要 一个额外的空间存储排序后的值
{
    // if (start == end) {
    if (start >= end) {//递归退出条件
        return;
    }

    int mid = start + (end - start) / 2;
    //求元素个数中位数，将其分成两半分别递归进行排序
    int low1 = start, low2 = mid + 1;
    int high1= mid, high2 = end;

    MergeSort(nums, tmp, low1, high1);
    MergeSort(nums, tmp, low2, high2);
    //排序完成后，有两个有序的子序列，将其进行有序合并

    int index = start;
    //将start-end范围内的值进行排序，保存到tmp数组中
    while (low1 <= high1 && low2 <= high2) {
        //当有一个序列已经为空时退出
        tmp[index++] = nums[low1] < nums[low2] ? nums[low1++] : nums[low2++];
    }

    while (low1 <= high1) {
        //将剩下的另一个序列直接拷贝到tmp数组中
        tmp[index++] = nums[low1++];
    }

    while (low2 <= high2) {
        //将剩下的另一个序列直接拷贝到tmp数组中
        tmp[index++] = nums[low2++];
    }

    for (index = start; index <= end; ++index) {
        //将tmp数组拷贝到原数组中
        nums[index] = tmp[index];
    }
}

void MergeSort2(vector<int>& nums, vector<int>& tmp, int start, int end)
{
    if (start == end) {
        return;
    }

    int mid = start + ((end - start) >> 1);
    int low1 = start, low2 = mid + 1;
    int high1 = mid, high2 = end;

    MergeSort2(tmp, nums, low1, high1);//减少了tmp向nums的赋值部分
    //两个值放反了就是上面第一种需要在最后把tmp赋值到nums的方法
    MergeSort2(tmp, nums, low2, high2);

    int index = start;
    while (low1 <= high1 && low2 <= high2) {
        tmp[index++] = nums[low1] < nums[low2] ? nums[low1++] : nums[low2++];
    }

    while (low1 <= high1) {
        tmp[index++] = nums[low1++];
    }
    
    while (low2 <= high2) {
        tmp[index++] = nums[low2++];
    }
}

void test()
{
    vector<int> nums = {1, 5, 0, 9, 7, 4, 3, 6, 2, 5, 1, 8};
    vector<int> tmp(nums);//拷贝构造

    // MergeSort(nums, tmp, 0, nums.size() - 1);
    MergeSort2(nums, tmp, 0, nums.size() - 1);
    nums.assign(tmp.begin(), tmp.end());//到最后tmp数组是排序好的，要赋值到原数组中

    for (auto& elem : nums) {
        cout << elem << " ";
    }
    cout << endl;
}

int main()
{
    test();
    return 0;
}

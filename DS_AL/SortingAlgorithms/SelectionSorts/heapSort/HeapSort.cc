/*
*  File    HeapSort.cc
*  Author  WayneGreat
*  Date    2021-08-30  22:20:39
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::swap;

//公式
//parent = (i - 1) / 2 =>向下取整
//leftChild = 2i + 1
//rightChild = 2i + 2

void heapify(vector<int>& tree, int unsortTreeLen, int parent) 
{//对有一定顺序的的堆从上到下的大根堆化
    if (parent >= unsortTreeLen) {//递归出口
        return;
    }
    int leftChild = 2 * parent + 1;
    int rightChild = 2 * parent + 2;
    int max = parent;
    if (leftChild < unsortTreeLen && tree[leftChild] > tree[max]) {
        max = leftChild;
    }
    if (rightChild < unsortTreeLen && tree[rightChild] > tree[max]) {
        max = rightChild;
    }
    if (max != parent) {
        swap(tree[max], tree[parent]);
        heapify(tree, unsortTreeLen, max);
    }
}

void buildHeap(vector<int>& tree) 
{//第一次建立大根堆
    int lastNode = tree.size() - 1;
    int parent = (lastNode - 1) / 2;
    for (int idx = parent; idx >= 0; --idx) {
        //从最后一个结点的父节点开始往下的大根堆化，完成后接着上一个结点的大根堆化
        heapify(tree, tree.size(), idx);
    }
}

void heapSort(vector<int>& tree) 
{
    buildHeap(tree);//建大根堆
    for (int idx = tree.size() - 1; idx >= 0; --idx) {
        //头与尾交换tree.size()-1次
        swap(tree[idx], tree[0]);
        //在进行从上到下的大根堆化
        heapify(tree, idx, 0);
    }
}

void test()
{
    vector<int> nums = {2, 9, 4, 6, 5, 1, 8, 3, 7};

    heapSort(nums);

    for (auto& elem : nums) {
        cout << elem << " ";
    }
    cout << endl;
}

int main()
{
    test();
    return 0;
}











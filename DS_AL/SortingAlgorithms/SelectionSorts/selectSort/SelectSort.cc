/*
*  File    SelectSort.cc
*  Author  WayneGreat
*  Date    2021-08-26  22:29:41
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::swap;

void SelectSort(vector<int>& nums)
{
    int len = nums.size();
    int minindex = 0;

    for(int idx = 0; idx < len; ++idx)
    {
        // int minindex = idx;
        minindex = idx;
        
        for(int j = idx + 1; j < len; ++j)
        {
            if(nums[j] < nums[minindex])
            {
                minindex = j;//记录最小元素的下标
            }
        }
        //交换两元素的位置，将最小的元素放到已有序对列的末尾
        swap(nums[idx], nums[minindex]);
    }
}

void Select(vector<int> &nums)
{
   int len = nums.size();
   for (int idx = len - 1; idx >= 0; --idx) {
       int maxIndex = idx;
       for (int j = idx - 1; j >= 0; --j) {
           if (nums[j] > nums[maxIndex]) {
               maxIndex = j;//记录最大元素的下标
           }
       }
       swap(nums[idx], nums[maxIndex]);
   } 
}

void test()
{
    vector<int> nums = {1, 4, 5, 2, 6, 7, 5, 8, 0, 9};

    SelectSort(nums);
    // Select(nums);

    for(auto &elem : nums)
    {
        cout << elem << " ";
    }
    cout << endl;
}

int main()
{
    test();
    return 0;
}

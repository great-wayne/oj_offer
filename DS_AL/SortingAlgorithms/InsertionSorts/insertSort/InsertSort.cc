/*
*  File    InsertSort.cc
*  Author  WayneGreat
*  Date    2021-08-27  17:22:25
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

//时间复杂度 O(n^2) 空间 O(1)，稳定排序，原地排序
void Insertsort(vector<int>& nums)
{
    int len = nums.size();

    for (int idx = 1; idx < len; ++idx) {
        if (nums[idx] < nums[idx - 1]) {
            //待插入的值小于有序序列中最后一个元素，需要将其前方比它大的元素后移

            int j = idx - 1;//j为需要后移元素的下标
            int insertNum = nums[idx];//保存哨兵

            while(j >= 0 && insertNum < nums[j]) {//后移操作
                //将j下标的元素后移，j指向前一个元素下标
                nums[j + 1] = nums[j];
                --j;
            }

            nums[j + 1] = insertNum;//插入对应空位置
        }
    }
}

void test()
{
    vector<int> nums = {1, 5, 4, 6, 9, 5, 3, 2, 0, 8, 7};

    Insertsort(nums);

    for (auto& elem : nums) {
        cout << elem << " ";
    }
    cout << endl;
}

int main()
{
    test();
    return 0;
}

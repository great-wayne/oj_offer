/*
*  File    ShellSort.cc
*  Author  WayneGreat
*  Date    2021-08-28  12:01:44
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void ShellSortCore(vector<int>& nums, int gap, int idx)
//插入排序核心思想实现
{
    int insertNum = nums[idx];//保存待插入的值
    int j = idx - gap;//从前面一个值开始后移

    while(j >= 0 && insertNum < nums[j]) {
        nums[j + gap] = nums[j];
        j -= gap;
    }

    nums[j + gap] = insertNum;//插入到空位置
}

void ShellSort(vector<int>& nums)
{
    int len = nums.size();

    for (int gap = len / 2; gap > 0; gap /= 2) {
        //将数组进行分组，间隔为gap，循环不断缩小gap直到为1
        for (int idx = gap; idx < len; ++idx) {
            //每组又进行插入排序，形成局部有序
            ShellSortCore(nums, gap, idx);
        }
    }
}

void test()
{
    vector<int> nums = {1, 9, 0, 3, 6, 5, 2, 4, 7, 2, 8};

    ShellSort(nums);

    for (auto& elem : nums) {
        cout << elem << " ";
    }
    cout << endl;
}

int main()
{
    test();
    return 0;
}

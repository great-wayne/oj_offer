/*
*  File    test.cc
*  Author  WayneGreat
*  Date    2021-09-07  15:41:49
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::swap;

void Bubble(vector<int> &nums)
{
    bool flag = false;
    int len = nums.size();
    for (int idx = 0; idx < len - 1; ++idx) {
        flag = false;
        for (int j = 0; j < len - 1 - idx; ++j) {
            if (nums[j] > nums[j + 1]) {
                swap(nums[j], nums[j + 1]);
                flag = true;
            }
        }
        if (!flag) {
            break;
        }
    }
}

//============================
void Insert(vector<int> &nums)
{
    int len = nums.size();
    for (int idx = 1; idx < len; ++idx) {
        if (nums[idx] < nums[idx - 1]) {
            int j = idx - 1;
            int insertNum = nums[idx];
            while (j >=0 && insertNum < nums[j]) {
                nums[j + 1] = nums[j];
                --j;
            }
            nums[j + 1] = insertNum;
        }
    }
}

//========================
void ShellCore(vector<int> &nums, int idx, int gap)
{
    int j = idx - gap;
    int insertNum = nums[idx];
    while (j >= 0 && insertNum < nums[j]) {
        nums[j + gap] = nums[j];
        j -= gap;
    }
    nums[j + gap] = insertNum;
}

void Shell(vector<int> &nums)
{
    int len = nums.size();
    for (int gap = len / 2; gap > 0; gap /= 2) {
        for (int idx = gap; idx < len; ++idx) {
            ShellCore(nums, idx, gap);
        }
    }
}

//============================
void Select(vector<int> &nums)
{
   int len = nums.size();
   for (int idx = len - 1; idx >= 0; --idx) {
       int maxIndex = idx;
       for (int j = idx - 1; j >= 0; --j) {
           if (nums[j] > nums[maxIndex]) {
               maxIndex = j;//记录最大元素的下标
           }
       }
       swap(nums[idx], nums[maxIndex]);
   } 
}

//==========================
int Partition(vector<int> &nums, int start, int end)
{
    int endPos = start - 1;
    for (int idx = start; idx < end; ++idx) {
        if (nums[idx] < nums[end]) {
            ++endPos;
            if (endPos != idx) {
                swap(nums[idx], nums[endPos]);
            }
        }
    }
    ++endPos;
    swap(nums[endPos], nums[end]);

    return endPos;
}

void Quick(vector<int> &nums, int start, int end)
{
    if (start == end) {
        return;
    }

    int index = Partition(nums, start, end);

    if (index > start) {
        Quick(nums, start, index - 1);
    }

    if (index < end) {
        Quick(nums, index + 1, end);
    }
}

//=====================
void Merge(vector<int> &nums, vector<int> &temp, int start, int end)
{
    if (start >= end) {//递归退出条件
        return;
    }

    int mid = start + (end - start) / 2;
    int low1 = start, low2 = mid + 1;
    int high1 = mid - 1, high2 = end;

    Merge(nums, temp, low1, high1);
    Merge(nums, temp, low2, high2);

    // int index = 0; 错误
    int index = start;
    while (low1 <= high1 && low2 <= high2) {
        temp[index++] = nums[low1] < nums[low2] ? nums[low1++] : nums[low2++];
    }

    while (low1 <= high1) {
        temp[index++] = nums[low1++];
    }

    while (low2 <= high2) {
        temp[index++] = nums[low2++];
    }

    // for (int idx = 0; idx < nums.size(); ++idx) {
    //     nums[idx] = temp[idx];
    // } 错误

    for (index = start; index <= end; ++index) {
        nums[index] = temp[index];
    }
}

//====================
//parent = (i - 1) / 2
//leftChild = 2i + 1;
//rightChild = 2i + 2;

void heapify(vector<int> &tree, int unSortTreeLen, int parent)
{
    if (parent >= unSortTreeLen) {
        return;
    }
    int leftChild = 2 * parent + 1;
    int rightChild = 2 * parent + 2;
    int max = parent;
    if (leftChild < unSortTreeLen && tree[max] < tree[leftChild]) {
        max = leftChild;
    }
    if (rightChild < unSortTreeLen && tree[max] < tree[rightChild]) {
        max = rightChild;
    }
    if (max != parent) {
        swap(tree[max], tree[parent]);
        heapify(tree, unSortTreeLen, max);
    }
}

void buildHeap(vector<int> &tree)
{
    int lastNode = tree.size() - 1;
    int parent = (lastNode - 1) / 2;
    for (int idx = parent; idx >= 0; --idx) {
        heapify(tree, tree.size(), idx);
    }
}

void heapSort(vector<int> &tree)
{
    buildHeap(tree);
    for (int idx = tree.size() - 1; idx >= 0; --idx) {
        swap(tree[idx], tree[0]);
        heapify(tree, idx, 0);
    }
}
//====================
/*
1、计数排序
2、桶排序
3、基数排序
*/
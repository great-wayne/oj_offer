/*
 *  File    Swap.cc
 *  Author  WayneGreat
 *  Date    2021-08-26  17:18:00
 *  Describe 
 */

#include <iostream>

using std::cout;
using std::endl;

namespace wd
{

//上述 swap在stl_algobase.h中的实现，使用模板实现swap 
template <typename T>
inline void
swap(T &a, T &b)
{
    T tmp = a;
    a = b;
    b = tmp;
}

}// end of namespace wd


/*
*  File    bst.cc
*  Author  WayneGreat
*  Date    2021-09-15  17:09:07
*  Describe 
*/

#include <iostream>
#include <vector>
#include <stack>
#include <queue>

using std::cout;
using std::endl;
using std::vector;
using std::stack;
using std::queue;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

//前序遍历
class Solution
{
public:
    vector<int> preorderTraversal(TreeNode *root)
    {
        //根左右，递归
        if (root == nullptr) return {};
        _ret.emplace_back(root->val);
        preorderTraversal(root->left);
        preorderTraversal(root->right);
        return _ret;
    }

    vector<int> preorderTraversal2(TreeNode *root)
    {
        //迭代
        if (root == nullptr) return {};
        TreeNode *pNode = root;
        while (pNode != nullptr || !_stk.empty()) {
            while (pNode != nullptr) {//根左
                _ret.emplace_back(pNode->val);
                _stk.emplace(pNode);//保存根节点，用于返回到根节点遍历右子树
                pNode = pNode->left;
            }
            pNode = _stk.top();//获取根节点
            _stk.pop(); 
            pNode = pNode->right;//右
        }
        return _ret;
    }

private:
    vector<int> _ret;
    stack<TreeNode*> _stk;
};

//中序遍历
class Solution2
{
public:
    vector<int> inorderTraversal(TreeNode *root)
    {
        //递归，左根右
        if (root == nullptr) return {};
        inorderTraversal(root->left);
        _ret.emplace_back(root->val);
        inorderTraversal(root->right);
        return _ret;
    }

    vector<int> inorderTraversal2(TreeNode *root)
    {
        //迭代
        if (root == nullptr) return {};
        TreeNode *pNode = root;
        while (pNode != nullptr || !_stk.empty()) {
            while (pNode != nullptr) {//左
                _stk.emplace(pNode);
                pNode = pNode->left;
            }
            pNode = _stk.top();
            _stk.pop();
            _ret.emplace_back(pNode->val);//根
            pNode = pNode->right;//右
        }
        return _ret;
    }

private:
    vector<int> _ret;
    stack<TreeNode*> _stk; 
};

//后序遍历
class Solution3
{
public:
    vector<int> postorderTraversal(TreeNode *root)
    {
        //递归，左右根
        if (root == nullptr) return {};
        postorderTraversal(root->left);
        postorderTraversal(root->right);
        _ret.emplace_back(root->val);
        return _ret;
    }

    vector<int> postorderTraversal2(TreeNode *root)
    {
        if (root == nullptr) return {};
        TreeNode *pNode = root;
        TreeNode *pVisitedNode = nullptr;
        while (pNode != nullptr || !_stk.empty()) {
            while (pNode != nullptr) {
                _stk.emplace(pNode);
                pNode = pNode->left;
            }
            pNode = _stk.top();//获取根节点位置
            if (pNode->right == nullptr 
            || pVisitedNode == pNode->right) {
                //根节点右孩子遍历完
                _ret.emplace_back(pNode->val);//根节点插入到序列中
                _stk.pop();
                pVisitedNode = pNode;//设置下一次节点访问不需要访问此根节点
                pNode = nullptr;//置空后，获取栈顶节点
            }
            else {
                pNode = pNode->right;//还没遍历右孩子
            }
        }
        return _ret;
    }

private:
    vector<int> _ret;
    stack<TreeNode*> _stk;
};

//层序遍历
class Solution4
{
public:
    vector<vector<int>> levelOrder(TreeNode *root)
    {
        if (root == nullptr) return {};
        _que.push(root);//放入根节点
        int currentLevelSize = 0;//当前层的节点个数
        while (!_que.empty()) {//循环全部节点
            currentLevelSize = _que.size();
            _ret.emplace_back(vector<int>());//每层用一个数组保存
            for (int idx = 1; idx <= currentLevelSize; ++idx) {
                auto pNode = _que.front();//获取头节点，并出队
                _que.pop();
                _ret.back().emplace_back(pNode->val);//在创建的新数组中插入
                if (pNode->left) _que.push(pNode->left);//左子树入队
                if (pNode->right) _que.push(pNode->right);//右子树入队
            }
        }
        return _ret;
    }

private:
    vector<vector<int>> _ret;
    queue<TreeNode*> _que;
};
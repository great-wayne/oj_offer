//#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"
#define N 10

//确定数量的层次建树
int main(){
	pTreeNode_t pRoot = NULL;
    char c[] = "ABCDEFGHIJ";
	pTreeNode_t pArr[N];//用于存储树结点的地址
	for (int i = 0; i < N; ++i) {//申请空间，并将val初始化
		pArr[i] = (pTreeNode_t)calloc(1,sizeof(TreeNode_t));
		pArr[i]->val = c[i];
	}
	pRoot = pArr[0];//将第一个树结点插入到树中
	for (int i = 0, j = 1; j < N; ++j) {
		// i 是要修改指针域的结点的下标
		// j 表示要被插入树中的结点的下标
		if (pArr[i]->pLeft == NULL) {//插入到左孩子
			pArr[i]->pLeft = pArr[j];
		}
		else {//插入到右孩子
			pArr[i]->pRight = pArr[j];
			++i;
		}
	}
    system("pause");
    return 0;
}

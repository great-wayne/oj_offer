typedef struct treeNode_s {
	char val;
	struct treeNode_s *pLeft;
	struct treeNode_s *pRight;
} TreeNode_t, *pTreeNode_t;
/* typedef struct queueNode_s {
	pTreeNode_t treeNode;
	struct queueNode_s *pNext;
} QueueNode_t, *pQueueNode_t; */
/* void preOrder(pTreeNode_t root);
void buildBinaryTree(pTreeNode_t *ppRoot, pQueueNode_t *ppFront, pQueueNode_t *ppRear, char ch);
 */
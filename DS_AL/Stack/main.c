#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SIZE 10

int isEmpty(int top)
{
    if (top == -1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void push(int *stack, int *top, int data)
{
    stack[++(*top)] = data;
}

void pop(int *top)
{
    if (isEmpty(*top))
    {
        fprintf(stderr, "stack is empty\n");
    }
    else
    {
        --(*top);
    }
}

int gettop(int *stack, int top)
{
    if (isEmpty(top))
    {
        fprintf(stderr, "stack is empty\n");
        return -999;
    }
    else
    {
        return stack[top];
    }
}

int main()
{
    int stack[SIZE] = {0};
    int top = -1;
    push(stack, &top, 2);
    push(stack, &top, 5);
    push(stack, &top, 3);
    push(stack, &top, 6);
    pop(&top);
    printf("%d\n", gettop(stack, top));
    pop(&top);
    printf("%d\n", gettop(stack, top));
    push(stack, &top, 4);
    printf("%d\n", gettop(stack, top));
    system("pause");
    return 0;
}

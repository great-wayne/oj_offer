/*
*  File    reorderList.cc
*  Author  WayneGreat
*  Date    2021-09-09  16:43:26
*  Describe 
*/

/*
题目说了交换吧，所以反对用栈啊或者双向队列重新拷贝之类的方法，应该是在原来链表上操作
操作：寻找链表中点 + 链表逆序 + 合并链表
1.快慢指针找到中点 2.拆成两个链表 3.遍历两个链表，后面的塞到前面的“缝隙里”
*/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

#include <iostream>

using std::cout;
using std::endl;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution {
public:
    void reorderList(ListNode *head)
    {
        if (head == nullptr) {
            return;
        }

        ListNode *mid = midNode(head);//获取中间节点位置

        ListNode *needReverse = mid->next;
        mid->next = nullptr;//断开成两条链
        needReverse =  reverseList(needReverse);//对后面的链进行反转

        mergeList(head, needReverse);//将两条链合并
    }

private:
    ListNode *midNode(ListNode *head)
    {
        ListNode *slow = head;
        ListNode *fast = head;
        while (fast != nullptr && fast->next != nullptr) {
            slow = slow->next;
            fast = fast->next->next;
        }

        return slow;
    }

    ListNode *reverseList(ListNode *head)
    {
        ListNode* pReverseListHead = nullptr;
        ListNode* pCur = head;
        ListNode* pPre = nullptr;

        ListNode *pNext = nullptr;
        while (pCur != nullptr) {
            //ListNode* pNext = pCur->next;
            pNext = pCur->next;
            
            if (pNext == nullptr) {
                pReverseListHead = pCur;
            }

            pCur->next = pPre;
            pPre = pCur;
            pCur = pNext;
        }

        return pReverseListHead;
    }

    void mergeList(ListNode *l1, ListNode *l2)
    {
        ListNode *tmp1 = nullptr;
        ListNode *tmp2 = nullptr;
        while (l1 != nullptr && l2 != nullptr) {
            tmp1 = l1->next;//保存后方位置
            tmp2 = l2->next;

            l1->next = l2;//先l1连l2
            l1 = tmp1;

            l2->next = l1;
            l2 = tmp2;
        }
    }
};
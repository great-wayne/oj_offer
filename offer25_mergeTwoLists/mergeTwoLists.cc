/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

//合并有序链表 
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}
};

//递归
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        if (l1 == nullptr)
        {
            return l2;
        }
        else if (l2 == nullptr)
        {
            return l1;
        }

        ListNode *pMergeHead = nullptr;

        if (l1->val < l2->val)
        {
            pMergeHead = l1;
            pMergeHead->next = mergeTwoLists(l1->next, l2);
        }
        else
        {
            pMergeHead = l2;
            pMergeHead->next = mergeTwoLists(l1, l2->next);
        }

        return pMergeHead;
    }
}; 

//非递归
class Solution2
{
public:
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2){
        if (l1 == nullptr) return l2;
        if (l2 == nullptr) return l1;

        ListNode head(0);
        ListNode *pNode = &head;

        // ListNode *pHead = nullptr;
        while (l1 != nullptr && l2 != nullptr) {
            if (l1->val < l2->val) {
                pNode->next = l1;
                l1 = l1->next;
            }
            else
            {
                pNode->next = l2;
                l2 = l2->next;
            }
            pNode = pNode->next;
        }

        if (l1 != nullptr) {
            pNode->next = l1;
        }

        if (l2 != nullptr) {
            pNode->next = l2;
        }

        return head.next;
    }
};
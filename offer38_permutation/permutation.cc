/*
*  File    permutation.cc
*  Author  WayneGreat
*  Date    2021-09-14  12:14:27
*  Describe 
*/

/*
好题及难题
全排列：字符串或一串数字 => 回溯法（视频地址：https://www.bilibili.com/video/BV1dx411S7WR）
*/
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <unordered_map>

using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::swap;
using std::sort;
using std::unordered_map;

class Solution
{
public:
    vector<string> permutation(string s)
    {
        if (0 == s.size()) return vector<string>();
        permutationCore(s, 0, s.size() - 1);
        sort(_result.begin(), _result.end());//将字符串按字典序排序
        return _result;
    }

private:
    void permutationCore(string &s, int start, int end)
    {
        if (start == end) {//将排列加入到vector中
            _result.push_back(s);
            return;
        }
        unordered_map<int, int> visited;         //防止含有重复的字符时的重复排列
        for (int idx = start; idx <= end; ++idx) {//回溯法
            if (visited[s[idx]] == 1) {
                continue;
            }
            swap(s[idx], s[start]);
            //交换后，对start后的字符串进行排列
            permutationCore(s, start + 1, end);
            //排列完成后，回归到原来的交换前的状态
            swap(s[idx], s[start]);
            visited[s[idx]] = 1;
        }
    }

private:
    vector<string> _result;//排列
};
/*
*  File    twoSum.cc
*  Author  WayneGreat
*  Date    2021-09-15  15:50:12
*  Describe 
*/

#include <iostream>
#include <vector>
#include <unordered_map>

using std::cout;
using std::endl;
using std::vector;
using std::unordered_map;

class Solution
{
public:
    vector<int> twoSum(vector<int> &nums, int target)
    {
        for (int i = 0; i < nums.size(); ++i) {
            auto it = _hashTable.find(target - nums[i]);//在哈希表中找另一个数
            if (it != _hashTable.end()) {
                return {it->second, i};
            }
            _hashTable[nums[i]] = i;//记录此数及下标
        }
        return {};
    }

private:
    unordered_map<int, int> _hashTable;//保存数组中数值及下标
};
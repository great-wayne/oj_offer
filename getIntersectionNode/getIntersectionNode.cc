/*
*  File    getIntersectionNode.cc
*  Author  WayneGreat
*  Date    2021-09-15  15:30:04
*  Describe 
*/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

#include <iostream>

using std::cout;
using std::endl;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB)
    {
        if(headA == nullptr || headB == nullptr) return nullptr;
        ListNode *pA = headA;
        ListNode *pB = headB;
        while (pA != pB) {//双指针法 => 遍历两个链表找相交
            pA = pA == nullptr ? headB : pA->next;
            pB = pB == nullptr ? headA : pB->next;
        }
        return pA;
    }
};
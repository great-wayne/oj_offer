/*
*  File    MinStack.cc
*  Author  WayneGreat
*  Date    2021-09-12  16:03:41
*  Describe 
*/

#include <iostream>
#include <stack>

using std::cout;
using std::endl;
using std::cerr;
using std::stack;

class MinStack {
public:
    /** initialize your data structure here. */
    MinStack()
    {}

    void push(int x)
    {
        _data.push(x);

        if (_min.size() == 0 || _min.top() > x) {//辅助栈为空或x比辅助栈的栈顶元素更大
            _min.push(x);
        }
        else {
            _min.push(_min.top());
        }
    }

    void pop()
    {
        if (_data.size() > 0 && _min.size() > 0) {
            _data.pop();
            _min.pop();
        }
        else {
            cerr << "stack is empty" << endl;
        }
    }

    int top()
    {
        return _data.top();
    }

    int min()
    {
        return _min.top();
    }

private:
    stack<int> _data;//数据栈
    stack<int> _min;//辅助栈
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(x);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->min();
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.h"

int main() {
	int arr[] = { 2, 3, 1, 0, 2, 5, 3 };
	int res = findRepeatNumber(arr, 7);
	//int res = duplicate(arr, 7);
	printf("%d\n", res);
	system("pause");
	return 0;
}

//暴力法：先排序，再扫描数组
//方法一：时间优先，哈希数组 时间O(n),空间O(n)
int findRepeatNumber(const int* nums,int numsSize) {
	if (nums == NULL) {
		fprintf(stderr, "arry is null\n");
		return -1;
	}//判空
	for (int i = 0; i < numsSize; ++i) {
		if (nums[i]<0 || nums[i]>numsSize - 1) {
			fprintf(stderr, "numbers is error\n");
			return -1;
		}
	}//数据是否合法
	int *hashTable = (int *)calloc(numsSize, sizeof(int));
	int res;
	for (int i = 0; i < numsSize; ++i) {
		hashTable[nums[i]]++;
		if (hashTable[nums[i]] > 1) {
			res = nums[i];
		}
	}
	return res;//返回重复多次且最大的值
}

//方法二：空间优先，原地排序数组 时间O(n)，空间O(1)
int duplicate(int* nums, const int numSize) {
	if (nums == NULL) {
		fprintf(stderr, "arry is null\n");
		return -1;
	}//判空
	for (int i = 0; i < numSize; ++i) {
		if (nums[i]<0 || nums[i]>numSize - 1) {
			fprintf(stderr, "numbers is error\n");
			return -1;
		}
	}//数据是否合法
	int temp;
	for (int i = 0; i < numSize; ++i) {
		while (nums[i] != i) {
			if (nums[i] == nums[nums[i]]) {//重复
				return nums[i];//返回第一个重复的数
			}
			temp = nums[i];
			nums[nums[i]] = temp;
			nums[i] = nums[nums[i]];
		}
	}
}

/*
*  File    copyRandomList.cc
*  Author  WayneGreat
*  Date    2021-09-13  14:04:16
*  Describe 
*/

/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* next;
    Node* random;
    
    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};
*/

#include <iostream>
#include <unordered_map>

using std::cout;
using std::endl;
using std::unordered_map;

class Node {
public:
    int val;
    Node* next;
    Node* random;
    
    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};

class Solution
{
public:
    Node *copyRandomList(Node *head)
    {
        if (head == nullptr) return nullptr;
        for (auto p = head; p != nullptr; p = p->next) {
            //遍历链表，创建新节点并保存到哈希表中
            _hashTable[p] = new Node(p->val);//new的三个步骤！
        }
        for (auto p = head; p != nullptr; p = p->next) {//迭代法
            _hashTable[p]->next = _hashTable[p->next];
            _hashTable[p]->random = _hashTable[p->random];
        }
        return _hashTable[head];
    }
private:
    unordered_map<Node*, Node*> _hashTable;//保存{指向旧节点的指针，指向新节点的指针}
};
/*
*  File    thread.cc
*  Author  WayneGreat
*  Date    2021-09-09  20:49:08
*  Describe 
*/

//交替打印ABC
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

using std::cout;
using std::endl;
using std::thread;
using std::mutex;
using std::unique_lock;
using std::condition_variable;

namespace wd
{
mutex myMutex;
condition_variable cv;
int flag = 0;

void printA()
{
    unique_lock<mutex> lk(myMutex);//上锁，使用完毕后自动解锁
    int count = 0;
    while (count < 10) {//打印十次
        while (flag != 0) {//当信号不为0时执行
            cv.wait(lk);
            //解锁进入睡眠，等待cv.notify信号后被唤醒
        }
        cout << "thread 1 : A" << endl;//打印A
        flag = 1;//将信号置为1，即接下来打印B
        cv.notify_all();//唤醒全部进入等待的条件变量
        count++;
    }
    cout << "my thread 1 finish" << endl;
}

void printB()
{
    unique_lock<mutex> lk(myMutex);
    for (int idx = 0; idx < 10; ++idx) {
        while (flag != 1) {
            cv.wait(lk);
        }
        cout << "thread 2 : B" << endl;
        flag = 2;
        cv.notify_all();
    }
    cout << "my thread 2 finish" << endl;
}

void printC()
{
    unique_lock<mutex> lk(myMutex);
    for (int idx = 0; idx < 10; ++idx) {
        while (flag != 2) {
            cv.wait(lk);
        }
        cout << "thread 3 : C" << endl;
        flag = 0;
        cv.notify_all();
    }
    cout << "my thread 3 finish" << endl;
}

}

void test()
{
    thread th1(wd::printA);
    thread th2(wd::printB);
    thread th3(wd::printC);

    th1.join();
    th2.join();
    th3.join();

    cout << "main thread" << endl;
}

int main()
{
    test();
    return 0;
}
//g++ thread.cc -o thread -lpthread


cc:=g++
LIST:=$(wildcard *.cc)
EXE:=$(patsubst %.cc,%,$(LIST))
all:$(EXE)
%:%.cc
	$(cc) $^ -o $@
.PHONY:rebuild clean
clean:
	$(RM) $(EXE)
rebuild:clean all

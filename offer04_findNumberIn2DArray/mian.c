#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.h"

int main() {
	int arr[][5] = {
		{ 1,  4,  7, 11, 15},
		{ 2,  5,  8, 12, 19},
		{ 3,  6,  9, 16, 22},
		{10, 13, 14, 17, 24},
		{18, 21, 23, 26, 30},
		{20, 25, 27, 28, 33}
	};
	//int res = Find(arr, 5, 5, 5);
	//int res = Find(arr, 5, 5, 20);
	//sizeof(arr) / sizeof(int) 元素总个数
	int row_size = sizeof(arr) / sizeof(int) / (sizeof(arr[0]) / sizeof(int));//行数
	int *col_size = sizeof(arr[0]) / sizeof(int);//列数
	int *pArr[sizeof(arr) / sizeof(int) / (sizeof(arr[0]) / sizeof(int))];//基类型为int的指针数组
	for (int i = 0; i < sizeof(arr) / sizeof(int) / (sizeof(arr[0]) / sizeof(int)); ++i) {
		pArr[i] = arr[i];//保存每行首地址，传入函数时 * 可以退化成 **（二级指针）
	}
	int res = findNumberIn2DArray(pArr, row_size, &col_size, 0);
	if (res) {
		printf("True\n");
	}
	else {
		printf("False\n");
	}
	system("pause");
	return 0;
}

//matrixSize->row,matrixColSize->colums
int findNumberIn2DArray(int** matrix, int matrixSize, int* matrixColSize, int target) {//leetcode上
	if (matrix == NULL || matrixSize <= 0 || *matrixColSize <= 0) {
		fprintf(stderr, "matrix is null\n");
		return 0;
	}
	int found = 0;
	int row = 0;
	int column = *matrixColSize - 1;
	while (row < matrixSize && column >= 0) {
		if (matrix[row][column] == target) {
			return 1;
		}
		else if (matrix[row][column] < target) {
			++row;
		}
		else {
			--column;
		}
	}
	return found;
}

int Find(int *matrix, int rows, int columns, int number) {//书上
	if (matrix == NULL || rows <= 0 || columns <= 0) {
		fprintf(stderr, "matrix is null\n");
		return 0;
	}
	int found = 0;//bool值
	int row = 0;
	int column = columns - 1;
	while (row < rows && column >= 0) {
		if (matrix[row * columns + column] == number) {//从右上角比较，相等返回1
			return 1;
		}
		else if (matrix[row * columns + column] < number) {//小于目标往下
			++row;
		}
		else {//大于目标往左
			--column;
		}
	}
	return found;//遍历所有没找到返回0
}
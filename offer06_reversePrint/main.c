#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.h"
#define N 10000

int main() {
	struct ListNode *pHead = NULL;
	int val;
	int count = 0;
	while (scanf("%d", &val) != EOF) {
		struct ListNode *pNew = (struct ListNode *)calloc(1, sizeof(struct ListNode));
		pNew->val = val;
		++count;
		if (pHead == NULL) {
			pHead = pNew;
		}
		else {//头插法
			pNew->next = pHead;
			pHead = pNew;
		}
	}
	//打印原始链表
	struct ListNode *pCur = pHead;
	while (pCur!=NULL) {
		printf("%d ", pCur->val);
		pCur = pCur->next;
	}
	puts("");

	////打印反向的数组
	//int returnSize = 0;
	//int *numArr = reversePrint(pHead, &returnSize);
	//for (int i = 0; i < returnSize; ++i) {
	//	printf("%d ", numArr[i]);
	//}
	//puts("");

	reversePrint_Recursively(pHead);
	puts("");

	system("pause");
	return 0;
}

int* reversePrint(struct ListNode* head, int* returnSize) {//returnSize为需要返回的长度
	//顺序栈
	int *top = (int *)calloc(N, sizeof(int));
	//int stack[100] = { 0 };
	//int top = -1;//栈顶下标
	//入栈
	struct ListNode *pCur = head;
	int len = 0;
	while (pCur != NULL) {
		top[len] = pCur->val;
		pCur = pCur->next;
		++len;
	}
	*returnSize = len;//返回数组长度，用于后续遍历数组
	//出栈，存入数组
	int i = 0;
	int *ret = (int *)calloc(len, sizeof(int));
	--len;
	while (len != -1) {
		ret[i] = top[len];
		--len;
		++i;
	}
	return ret;//返回指向数组的指针
}

void reversePrint_Recursively(struct ListNode* phead) {//递归
	if (phead != NULL) {
		if (phead->next != NULL) {
			reversePrint_Recursively(phead->next);
		}
		printf("%d ", phead->val);
	}
}
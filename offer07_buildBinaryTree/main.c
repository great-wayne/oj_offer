#include <stdio.h>
#include <stdlib.h>

struct TreeNode
{
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

//preorder指向前序遍历数组的头结点，preorderSize为数组大小
//inorder指向中序遍历数组的头结点，inorderSize为数组大小
//返回结构体指针类型
struct TreeNode *buildTree(int *preorder, int preorderSize, int *inorder, int inorderSize)
{
    if(preorderSize == 0 || inorderSize == 0){//判[]
        return NULL;
    }

    struct TreeNode *pRoot = (struct TreeNode*)calloc(1, sizeof(struct TreeNode));
    pRoot->val = preorder[0];
    pRoot->left = NULL;
    pRoot->right = NULL;//申请第一个结点，并初始化val为前序遍历的第一个数字和左右指针为null

    if(preorder == (preorder + preorderSize)){
        if(inorder == (inorder + inorderSize) && *preorder == *inorder){//判一个结点的序列
            return pRoot;
        }
        else{//一个结点但数据不一致
            fprintf(stderr, "invaild input!\n");
            return NULL;
        }
    }
   
    //在中序遍历中找到根节点
    int leftLength = 0;//计算左子树的序列长度
    int *rootInorder = inorder;
    int *endorder = inorder + inorderSize - 1;
    while(rootInorder < endorder && *rootInorder != pRoot->val){
        ++rootInorder;
        ++leftLength;
    }
    if(rootInorder == endorder && *rootInorder != pRoot->val){//最后一个还是不等于，则报错
        fprintf(stderr, "invalid input!\n");
    }

    if(leftLength > 0){//左子树不为空
        pRoot->left = buildTree(preorder + 1, leftLength, inorder, leftLength);
    }
    if(leftLength < preorderSize - 1){//右子树不为空
        pRoot->right = buildTree(preorder + leftLength + 1, preorderSize - leftLength - 1,
            rootInorder + 1, inorderSize - leftLength - 1);
    }

    return pRoot;;
}
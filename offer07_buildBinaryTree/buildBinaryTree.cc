/*
*  File    buildBinaryTree.cc
*  Author  WayneGreat
*  Date    2021-09-12  09:36:07
*  Describe 
*/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

#include <iostream>
#include <vector>
#include <unordered_map>

using std::cout;
using std::endl;
using std::vector;
using std::unordered_map;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        unordered_map<int, int>hashmap;//使用hash表加速检索中序遍历序列中元素的下标
        for (int idx = 0; idx < inorder.size(); ++idx) {
            hashmap.insert({inorder[idx], idx});//将中序序列{元素，数组下标}加入到hashTable
        }
        return buildTreeCore(hashmap, preorder, 0, 0, inorder.size() - 1);//返回指向root节点指针
    }
private:
    TreeNode* buildTreeCore(unordered_map<int, int>& hashmap, vector<int>& pre, int low1, int low2, int high2) {
        //中序hash表(获取元素值在中序数组的下标)，前序数组(获取元素值)，前序root(用于建立节点)，中序strat和end(用于左右子树递归)

        // if (low1 == pre.size() || low2 > high2) {//递归退出条件
        if (low2 > high2) {//递归退出条件
            return nullptr;
        }
        TreeNode* root = new TreeNode(pre[low1]);//递归new每个节点用于返回其指针，接着上一层节点与此节点相连
        int index = hashmap[pre[low1]];//找此节点在中序的下标
        //按此节点在中序的下标位置分开两半，不断左右递归建立二叉树
        root->left = buildTreeCore(hashmap, pre, low1 + 1, low2, index - 1);
        root->right = buildTreeCore(hashmap, pre, (low1 + 1) + (index - low2), index + 1, high2);

        return root;
    }
};
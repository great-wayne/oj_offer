#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.h"

int main()
{
	char *str = "99999999999";
	printf("%d\n", StrtoInt(str));
	system("pause");
	return 0;
}

int StrtoInt(char *str)
{ //str: "123", "+123", "jkl/*", "   ", NULL, "123nl", "-1234", "1234-", "++", "-"
	if (str == NULL)
	{ //判空
		fprintf(stderr, "String is NULL\n");
		return -99999; //error number
	}
	if (!isInt_str(str))
	{ //是否合法
		fprintf(stderr, "Error Number String\n");
		return -99999;
	}
	int num = 0;
	char ch = 0; //记录正负号
	while (*str != '\0')
	{
		if (*str == '+' || *str == '-')
		{
			ch = *str;
			++str;
			continue;
		}
		num = num * 10 + *str - '0';
		++str;
	}
	if (ch == '-')
	{
		num = ~num + 1; //正转负：取反加一
		if (num > 0)
		{ //下溢出变正，溢出判断 ？
			fprintf(stderr, "Error：Number is too small\n");
			return -99999;
		}
	}
	else
	{
		if (num < 0)
		{ //上溢出变负
			fprintf(stderr, "Error: Number is too big\n");
			return -99999;
		}
	}
	//~(1<<32-1) == 2147483647
	//(1<<32-1) == -2147483648
	return num;
}

int isInt_str(char *str)
{
	int len = strlen(str);
	if (str[0] < '0' && (str[0] == '+' || str[0] == '-'))
	{ //第一个为符号
		if (len == 1)
		{ //判断长度
			return 0;
		}
		for (int i = 1; i < len; ++i)
		{ //判断后续是否为数字字符
			if (str[i] < '0' || str[i] > '9')
			{
				return 0;
			}
		}
		return 1;
	}
	else
	{
		for (int i = 0; i < len; ++i)
		{
			if (str[i] < '0' || str[i] > '9')
			{
				return 0;
			}
		}
		return 1;
	}
}
#include <stdlib.h>
#include <stdio.h>

typedef struct treeNode_s {
    int data;
    struct treeNode_s *pParent;
    struct treeNode_s *pLeft;
    struct treeNode_s *pRight;
}binaryTree_t, *pbinaryTree_t;


binaryTree_t *GetNext(pbinaryTree_t pNode) {
    
    if(pNode == NULL) {
        fprintf(stderr, "binary is null\n");
        return NULL;
    }

    pbinaryTree_t pNext = NULL;//用于返回pNode下一个结点的指针
    if(pNode->pRight != NULL) {//结点右子树不为空，找右子树中最左的结点
        pbinaryTree_t pright = pNode->pRight;
        while(pright->pLeft != NULL) {
            pright = pright->pLeft;
        }
        
        pNext = pright;
    }
    else if(pNode->pRight == NULL) {//结点无右结点，向上找的下一个结点为其父结点左边
        pbinaryTree_t pCur = pNode;
        pbinaryTree_t pPar = pNode->pParent;
        while(pPar != NULL && pPar->pRight == pCur){
            pCur = pPar;
            pPar = pPar->pParent;
        }

        pNext = pPar;
    }

    return pNext;
}
/*
*  File    pathSum.cc
*  Author  WayneGreat
*  Date    2021-09-13  12:40:03
*  Describe 
*/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution
{
public:
    vector<vector<int>> pathSum(TreeNode *root, int target)
    {
        if (root == nullptr) return vector<vector<int>>();
        pathSumCore(root, target);
        return _ret;
    }
private:
    //深度优先搜索
    void pathSumCore(TreeNode *root, int target)
    {
        // if (root == nullptr) return;
        _path.push_back(root->val);
        if (root->left == nullptr && root->right == nullptr && target == root->val) {
            //root指向叶子节点，且符合路径和
            _ret.push_back(_path);//加入到res中
        }
        else {
            if (root->left != nullptr) {
                pathSumCore(root->left, target - root->val);//减到叶节点符合该叶节点的值
            }
            if (root->right != nullptr) {
                pathSumCore(root->right, target - root->val);
            }
        }
        //返回到父节点，需要在保存路径的tmp中删除
        _path.pop_back();//如果不是引用方式，而是值传递，这一步是可以删掉的，是引用方式就必须要pop掉
    }
private:
    vector<int> _path;
    vector<vector<int>> _ret;
};

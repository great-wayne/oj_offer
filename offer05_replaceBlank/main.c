#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.h"
#define N 30

int main() {
	char str[N] = { 0 };
	strcpy(str, " We are happy. ");
	ReplaceBlank(str, N);
	//ReplaceBlank(NULL, N);
	printf("%s\n", str);
	system("pause");
	return 0;
}

//在新的字符串上进行替换，需要申请空间
char *replaceSpace(char *s) {
	if (s == NULL) {
		fprintf(stderr, "string is null\n");
		return NULL;
	}
	int len = strlen(s);//获得原字符数组长度
	char *resArry = (char *)calloc(3 * len + 1, sizeof(char));//申请空间,并初始化为0,多一位为'\0'使用
	int size = 0;//表示替换后字符串长度
	for (int i = 0; i < len; ++i) {
		if (s[i] == ' ') {
			resArry[size] = '%';
			resArry[size + 1] = '2';
			resArry[size + 2] = '0';
			size += 3;
		}
		else {
			resArry[size++] = s[i];
		}
	}
	return resArry;
}

//在原来的字符串上进行替换，并保证输入的字符串后面有足够多的内存空间
void ReplaceBlank(char string[], int length) {
	if (string == NULL || length <= 0) {//判空与字符串数组长度是否合法
		fprintf(stderr, "string is null\n");
		return;
	}
	int i = 0;
	int originLength = 0;//原长度
	int blankSpace = 0;//空格数
	while (string[i] != '\0') {
		++originLength;
		if (string[i] == ' ') {
			++blankSpace;
		}
		++i;
	}//计算空格与原字符串长度
	int newLength = originLength + blankSpace * 2;//新长度
	if (newLength > length) {
		fprintf(stderr, "newLength is too long than N\n");
		return;
	}
	while (originLength >= 0) {
		if (string[originLength] == ' ') {
			string[newLength--] = '0';
			string[newLength--] = '2';
			string[newLength--] = '%';
			--originLength;
		}
		else {
			string[newLength] = string[originLength];
			--newLength;
			--originLength;
		}
	}
}
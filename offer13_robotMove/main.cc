/*
题目：地上有一个 m 行 n 列的方格。一个机器人从坐标(0, 0)的格子开始移动，
它每一次可以向左、右、上、下移动一格，但不能进入行坐标和列坐标的数位之和大于 k 的格子。
例如，当 k 为 18 时，机器人能够进入方格(35, 37)，因为 3+5+3+7=18。
但它不能进入方格(35, 38)，因为 3+5+3+8=19。请问该机器人能够到达多少个格子？
*/

//回溯算法->解决物体或人在二维方格运动的问题，可运动的范围

#include <iostream>

using namespace std;


class Solution
{

    int getDigitSum(int number)
    {
        int sum = 0;
        while (number > 0)
        {
            sum += number % 10; //取出row的个位加和
            number /= 10;       //除去个位
        }

        return sum; //返回row或col的位数总和
    }

    bool check(int k, int rows, int cols, int row, int col, bool *visited)
    {
        if (row >= 0 && row < rows && col >= 0 && col < cols && getDigitSum(row) + getDigitSum(col) <= k && !visited[row * cols + col])
        {
            //检查row、col是否合法，row和col的位数之和是否小于等于k，此方格是否没有被访问过为false
            return true;
        }

        return false;
    }

    int movingCountCore(int k, int rows, int cols, int row, int col, bool *visited)
    {
        int count = 0;

        if (check(k, rows, cols, row, col, visited))
        { //先判断是否可以进入方格

            visited[row * cols + col] = true; 
            //标记当前方格已访问为true

            count = 1 + movingCountCore(k, rows, cols, row - 1, col, visited) +
                    movingCountCore(k, rows, cols, row, col - 1, visited) +
                    movingCountCore(k, rows, cols, row + 1, col, visited) +
                    movingCountCore(k, rows, cols, row, col + 1, visited);
            //使用递归的方式不断判断相邻（上下左右）的方格是否可以进入，进入后再判断相邻方格
        }

        return count; //返回可访问的方格数
    }

public:
    int movingCount(int m, int n, int k)
    {
        if (k < 0 || m < 0 || n < 0)
        //m->rows, n->clos, k
        {
            return 0;
        }

        bool *visited = new bool[m * n];
        //创建堆区记录是否访问了的方格

        for (int i = 0; i < m * n; ++i)
        {
            visited[i] = false;
        } //将数组初始化为false, 没有访问过
        //或者 => memset(visited, 0, m * n);

        int count = movingCountCore(k, m, n, 0, 0, visited);
        //从（0，0）开始在 m × n 的方格中运动

        delete[] visited; //释放堆区空间

        return count;
    }
};

void test()
{
    Solution p1;
    int ret = p1.movingCount(10, 10, 5);
    cout << "count = " << ret << endl;
}

int main()
{
    test();
    return 0;
}
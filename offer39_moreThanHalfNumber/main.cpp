/*
 * @lc app=leetcode.cn id=169 lang=cpp
 *
 * [169] 多数元素
 */

// @lc code=start

#include <vector>
#include <iostream>

using std::cerr;
using std::cout;
using std::endl;
using std::vector;

class Solution {
public:
    int majorityElement(vector<int>& nums) 
    {
        if(checkInvalidVector(nums))
        //检查vector是否invalid
        {
            return 0;
        }

        int length = nums.size();
        int middle = length >> 1;//除2
        int start = 0;
        int end = length - 1;

        int index = Partition(nums, start, end);

        while(index != middle)
        {
            if(index > middle)
            //新范围[start,index - 1]
            {
                end = index - 1;
                index = Partition(nums, start, end);
            }
            else
            //新范围[index + 1, end]
            {
                start = index + 1;
                index = Partition(nums, start, end);
            }
        }

        int result = nums[index];

        if(!checkMoreThanHalf(nums, result))
        {
            return 0;
        }

        return result;
    }

    void QuickSort(vector<int>& nums,int start,int end)
    {
        // cout << "QuickSort()" << endl;
        // int length = nums.size();
        if(start == end)//注意这里是判断是否相等
        {
            return;
        }

        int index = Partition(nums, start, end);
        if(index > start)
        {
            QuickSort(nums, start, index - 1);
        }
        if(index < end)
        {
            QuickSort(nums, index + 1, end);
        }
    }

private:
    bool checkInvalidVector(vector<int> & nums)
    //检查vector是否为无效
    {
        g_bInputInvalid = false;
        if(nums.empty())
        {
            g_bInputInvalid = true;
        }
        return g_bInputInvalid;
    }

    bool checkMoreThanHalf(vector<int> & nums, int result)
    //检查middle下标的元素是否为多数元素
    {
        int resultTimes = 0;
        for(int idx = 0; idx < nums.size(); ++idx)
        {
            if(nums[idx] == result)
            {
                ++resultTimes;
            }
        }

        bool isMoreThanHalf = true;
        if(resultTimes * 2 <= nums.size())
        //中间元素的两倍没有大于vector长度，即多数元素不成立
        {
            g_bInputInvalid = true;
            isMoreThanHalf = false;
        }

        return isMoreThanHalf;
    }


    int Partition(vector<int>& nums, int start, int end)
    {
        if(nums.empty())
        {
            cerr << "nums is empty!" << endl;
        }

        //任意选取一个元素与最后的元素交换位置..(待实现)

        //选择最后一个元素为分割点进行
        // int length = nums.size();
        int endpos = start - 1;

        for(int idx = start; idx < end; ++idx)
        {
            if(nums[idx] < nums[end])
            {
                ++endpos;
                if(endpos != idx)
                {
                    swap(&nums[endpos], &nums[idx]);
                    // cout << nums[endpos] 
                    //      << nums[idx] 
                    //      << endl;
                }
            }
        }
        ++endpos;
        swap(&nums[endpos], &nums[end]);

        return endpos;
    }

    void swap(int* lhs, int* rhs)
    {
        int tmp = *lhs;
        *lhs = *rhs;
        *rhs = tmp;
    }

private:
    bool g_bInputInvalid = false;
};

class Solution2
{
public:
    //在遍历数组时保存两个值=>一个数组中的数字，一个此数字的次数
    //遍历到下一个数字时，相同加1，不同减1，times为0时换一个数字，并把数字次数设置为1
    //最后的result为最后设置times为1的数字
    int majorityElement(vector<int>& nums) 
    {
        //选取第一个数字作为开始
        int result = nums[0];
        int resultTimes = 1;

        for(int idx = 1; idx < nums.size(); ++idx)
        //遍历全部数字，注意从下标为1的开始
        {
            if(resultTimes == 0)
            //换一个数字
            {
                result = nums[idx];
                resultTimes = 1;
            }
            else if(result == nums[idx])
            {
                ++resultTimes;
            }
            else
            {
                --resultTimes;
            }
        }

        return result;
    }
};
// @lc code=end

void test()
{
    // vector<int> nums = {1, 4, 3, 2, 2, 2, 5, 4, 2};
    // vector<int> nums = {1, 2, 3, 2, 2, 2, 5, 4, 2};
    vector<int> nums = {2, 2, 1, 1, 1, 2, 2};

    int length = nums.size();
    cout << "length = " << length << endl;

    Solution sl;//会改变原来的vector，需要pratition递归，用时较长
    // sl.QuickSort(nums, 0, length - 1);
    cout << "moreThanHalf number = " << sl.majorityElement(nums) << endl;

    Solution2 sl2;//不会改变原来的vector，利用数组的特点来求，用时较短
    cout << "moreThanHalf number = " << sl2.majorityElement(nums) << endl;

    // for(auto &elem : nums)
    // {
    //     cout << elem << " ";
    // }
    // cout << endl;
}

int main()
{
    test();
    return 0;
}
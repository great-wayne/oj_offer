/*
*  File    merge.cc
*  Author  WayneGreat
*  Date    2021-09-15  14:57:18
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

class Solution
{
public:
    void merge(vector<int> &nums1, int m, vector<int> &nums2, int n)
    {
        _tmp.reserve(nums1.size());
        int i = 0, j = 0;
        while (i < m && j < n) {
            if (nums1[i] < nums2[j]) {
                _tmp.emplace_back(nums1[i]);
                ++i;
            }
            else {
                _tmp.emplace_back(nums2[j]);
                ++j;
            }
        }

        while (i < m) {
            _tmp.emplace_back(nums1[i]);
            ++i;
        }

        while (j < n) {
            _tmp.emplace_back(nums2[j]);
            ++j;
        }

        nums1.clear();
        for (auto elem : _tmp) {
            nums1.emplace_back(elem);
        }
    }

private:
    vector<int> _tmp;
};
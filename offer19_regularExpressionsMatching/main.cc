#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::cerr;
using std::vector;

class Solution {
public:
    vector<int> exchange(vector<int>& nums) 
    {
        if(nums.empty())
        {
            cerr << "vector is empty. " << endl;
            return nums;
        }

        // int length = nums.size();
        auto iterBegin = nums.begin();
        auto iterEnd = nums.end() - 1;
        // cout << nums[0] << endl;
        // cout << *iterBegin << endl;
        // cout << *iterEnd << endl;

        while(iterBegin < iterEnd)
        {
            //将迭代器向后移动，直到它指向偶数
            //2 => 0010 & 0x1 => 0001 ===> 0000 代表为偶数，反之为奇数  
            while(iterBegin < iterEnd && (*iterBegin & 0x1) != 0)
            {
                ++iterBegin;
            }

            //将迭代器向前移动，直到它指向奇数
            while(iterBegin < iterEnd && (*iterEnd & 0x1) == 0)
            {
                --iterEnd;
            }

            //交换位置
            if(iterBegin < iterEnd)
            {
                int temp = *iterBegin;
                *iterBegin = *iterEnd;
                *iterEnd = temp;
            }
        }

        return nums;
    }
};

void test()
{
    Solution sl;
    vector<int> nums = {1, 10, 3, 2, 6, 7, 8, 5};

    sl.exchange(nums);
    for(auto &elem : nums)
    {
        cout << elem << " ";
    }
    cout << endl;
}

int main()
{
    test();
    return 0;
}

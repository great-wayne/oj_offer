#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::cerr;
using std::vector;

class Solution {
public:
    //为适应不同的问题需求，将其逻辑抽象出来，把判断标准变成一个函数指针，调用不同的函数实现不同的需求
    // vector<int> exchange(vector<int>& nums) 
    vector<int> exchange(vector<int>& nums, bool (*func)(int)) 
    {
        if(nums.empty())
        {
            cerr << "vector is empty. " << endl;
            return nums;
        }

        // int length = nums.size();
        auto iterBegin = nums.begin();
        auto iterEnd = nums.end() - 1;
        // cout << nums[0] << endl;
        // cout << *iterBegin << endl;
        // cout << *iterEnd << endl;

        while(iterBegin < iterEnd)
        {
            //将迭代器向后移动，直到它指向偶数
            //2 => 0010 & 0x1 => 0001 ===> 0000 代表为偶数，反之为奇数  
            // while(iterBegin < iterEnd && (*iterBegin & 0x1) != 0)
            while(iterBegin < iterEnd && !func(*iterBegin))
            {
                ++iterBegin;
            }

            //将迭代器向前移动，直到它指向奇数
            // while(iterBegin < iterEnd && (*iterEnd & 0x1) == 0)
            while(iterBegin < iterEnd && func(*iterEnd))
            {
                --iterEnd;
            }

            //交换位置
            if(iterBegin < iterEnd)
            {
                int temp = *iterBegin;
                *iterBegin = *iterEnd;
                *iterEnd = temp;
            }
        }

        return nums;
    }
};

//以奇偶数为标准，偶数放后面
bool isEven(int n)
{
    return (n & 1) == 0;
}

//以能否被3整除为标准，不能被整除的放后面
bool notDivide(int n)
{
    return (n % 3) != 0;
}

//以正负数为标准，正数的放后面
bool isPositive(int n)
{
    return n > 0; 
}

void test()
{
    Solution sl;
    // vector<int> nums = {1, 10, 3, 2, 6, 7, 8, 5};
    // vector<int> nums = {33, 44, 66, 10, 99, 22, 54};
    vector<int> nums = {10, 20, -1, 3, -6, -99, 32, 100};

    //解耦的好处是提高了代码的重用性，为功能扩展提供了便利
    // sl.exchange(nums, isEven);
    // sl.exchange(nums, notDivide);
    sl.exchange(nums, isPositive);
    for(auto &elem : nums)
    {
        cout << elem << " ";
    }
    cout << endl;
}

int main()
{
    test();
    return 0;
}

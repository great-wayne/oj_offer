/*
*  File    countPrimes.cc
*  Author  WayneGreat
*  Date    2021-09-13  09:58:10
*  Describe 
*/

#include <iostream>

using std::cout;
using std::endl;

//考虑质数的定义：在大于 1 的自然数中，除了 1 和它本身以外不再有其他因数的自然数。
//枚举法
class Solution
{
public:
    int countPrimes(int n)//计算n以内质数的数量
    {
        int cnt = 0;
        for (int i = 2; i < n; ++i) {
            cnt += isPrimes(i);
        }
        return cnt;
    }
private:
    bool isPrimes(int num)
    {
        for (int i = 2; i * i <= num; ++i) {
            //不难发现较小数一定落在 [2,√num] 的区间中
            //单次检查的时间复杂度从 O(n) 降低至了 O(√n)
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
};
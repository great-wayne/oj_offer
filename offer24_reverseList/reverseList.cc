/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}
};

//非递归
//三指针，pCur,pPre,pNext => 防断链
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode* pReverseListHead = nullptr;
        ListNode* pCur = head;
        ListNode* pPre = nullptr;

        while (pCur != nullptr) {
            ListNode* pNext = pCur->next;

            if (pNext == nullptr) {
                pReverseListHead = pCur;
            }

            pCur->next = pPre;
            pPre = pCur;
            pCur = pNext;
        }

        return pReverseListHead;
    }
};

//递归
class Solution2 {
public:
    ListNode* reverseList(ListNode* head) {
        return reverseListCore(head, nullptr);
    }
private:
    ListNode* reverseListCore(ListNode* pCur, ListNode* pPre) {
        if (pCur == nullptr) {//递归出口
            return pPre;
        }
        ListNode* pRes = reverseListCore(pCur->next, pCur);//对后方的结点进行递归
        pCur->next = pPre;// 修改节点引用指向
        return pRes;// 返回反转链表的头节点
    }
};


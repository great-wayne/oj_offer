/*
*  File    SpiralOrder.cc
*  Author  WayneGreat
*  Date    2021-09-12  14:06:47
*  Describe 
*/

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {//返回一个循环打印矩阵的数组
        vector<int> res;

        if (matrix.empty()) return res;//! 记得要先判断是否为空

        int top = 0, buttom = matrix.size() - 1;//上下边界
        int left = 0, right = matrix[0].size() - 1;//左右边界
        while (1) {
            for (int idx = left; idx <= right; ++idx) {//从左到右
                res.push_back(matrix[top][idx]);
            }
            if (++top > buttom) break;//超出边界时break

            for (int idx = top; idx <= buttom; ++idx) {//从上到下
                res.push_back(matrix[idx][right]);
            }
            if (--right < left) break;

            for (int idx = right; idx >= left; --idx) {//从右到左
                res.push_back(matrix[buttom][idx]);
            }
            if (--buttom < top) break;

            for (int idx = buttom; idx >= top; --idx) {//从下到上
                res.push_back(matrix[idx][left]);
            }
            if (++left > right) break;
        }
        return res;//返回一个循环数组
    }
};
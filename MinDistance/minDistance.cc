/*
*  File    minDistance.cc
*  Author  WayneGreat
*  Date    2021-09-05  16:25:56
*  Describe 最小距离，递归及动态规划的解法
*/

#include <iostream>
#include <string>
#include <algorithm>

using std::cout;
using std::endl;
using std::min;
using std::string;

namespace wd
{

//动态规划
class Solution
{
public:
    int minDistance(string word1, string word2) {
        int n = word1.size();
        int m = word2.size();

        if (0 == n * m) {//有一个字符串为空串
            return n + m;
        }

        //DP数组
        int arry[n + 1][m + 1];

        for (int i = 0; i < n + 1; ++i) {
            arry[i][0] = i;
        }

        for (int j = 0; j < m + 1; ++j) {
            arry[0][j] = j;
        }

        //计算所有的DP值
        for (int i = 1; i < n + 1; ++i) {
            for (int j = 1; j < m + 1; ++j) {
                int left = arry[i - 1][j] + 1;//执行删除操作
                int down = arry[i][j - 1] + 1;//执行插入操作
                int left_down = arry[i - 1][j - 1];//执行替换操作
                if (word1[i - 1] != word2[j - 1]) {
                    left_down += 1;
                }
                arry[i][j] = min(left, min(down, left_down));
            }
        }
        return arry[n][m];
    }
};

//递归解法
class Solution2
{
public:
    int minDistance(string word1, string word2)
    {
        int n = word1.size();
        int m = word2.size();

        if (0 == n * m) {//有一个字符串为空串
            return n + m;
        }

        if (word1.back() == word2.back()) {
            return minDistance(
                        word1.substr(0, word1.size() - 1),
                        word2.substr(0, word2.size() - 1)
                    );
        }

        return 1 + min(
                minDistance(word1, word2.substr(0, word2.size() - 1)),
                min(
                    minDistance(word1.substr(0, word1.size() - 1), word2),
                    minDistance(word1.substr(0, word1.size() - 1), 
                            word2.substr(0, word2.size() - 1)))
                );
    }
};


}// end of namespace wd

void test()
{
    wd::Solution sl1;
    wd::Solution2 sl2;
    cout << sl1.minDistance("apple", "oppa") << endl;
    cout << sl2.minDistance("apple", "oppa") << endl;
}

int main()
{
    test();
    return 0;
}

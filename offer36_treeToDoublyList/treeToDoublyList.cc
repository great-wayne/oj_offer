/*
*  File    treeToDoublyList.cc
*  Author  WayneGreat
*  Date    2021-09-14  10:53:08
*  Describe 
*/

/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;

    Node() {}

    Node(int _val) {
        val = _val;
        left = NULL;
        right = NULL;
    }

    Node(int _val, Node* _left, Node* _right) {
        val = _val;
        left = _left;
        right = _right;
    }
};
*/

#include <iostream>

using std::cout;
using std::endl;

class Node
{
public:
    int val;
    Node* left;
    Node* right;

    Node() {}

    Node(int _val) {
        val = _val;
        left = NULL;
        right = NULL;
    }

    Node(int _val, Node* _left, Node* _right) {
        val = _val;
        left = _left;
        right = _right;
    }
};

class Solution
{
public:
    Node *treeToDoublyList(Node *root)
    {
        if (root == nullptr) return root;//判空
        Node *pHead = Convert(root);
        Node *pTail = pHead;
        while (pHead->left) {
            //指针指向链表头
            pHead = pHead->left;
        }
        while (pTail->right) {
            //指针指向链表尾
            pTail = pTail->right;
        }
        //头尾相连
        pHead->left = pTail;
        pTail->right = pHead;
        return pHead;
    }
private:
    Node* Convert(Node *root)
    {
        if (root == nullptr) return root;
        //不断左右子树分别递归，构造双向链表
        if (root->left) {
            Node *left = Convert(root->left);//递归返回的是指向root->left的指针，此时left下的节点已经为双向链表
            while (left->right) left = left->right;//找到左子树的最大值的节点
            //相互连接
            left->right = root;
            root->left = left;
        }
        if (root->right) {
            Node *right = Convert(root->right);
            while (right->left) right = right->left;//找到右子树的最小值的节点
            //连接
            right->left = root;
            root->right = right;
        }
        return root;
    }
};

class Solution2
{
public:
    Node* treeToDoublyList(Node* root) 
    {
        if(root == nullptr) return nullptr;
        dfs(root);
        //头尾节点相连
        _pHead->left = _pPre;
        _pPre->right = _pHead;
        return _pHead;
    }
private:
    void dfs(Node* root) 
    {
        if(root == nullptr) return;
        dfs(root->left);//先到最左的节点
        if(_pPre == nullptr) {//第一次，即双向链表的头节点
            _pHead = root;
            _pPre = _pHead;
        } 
        else {//后续节点操作
            _pPre->right = root;//连接前后两个相邻节点
            root->left = _pPre;
            _pPre = root;//指向根节点，用于下一个节点的相连
        }
        dfs(root->right);//若有右孩子，继续找右子树下的最左节点（最小值）
    }
private:
    Node *_pHead;
    Node *_pPre;
};
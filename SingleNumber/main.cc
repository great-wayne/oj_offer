#include <unordered_map> //头文件
#include <vector>
#include <iostream>
using namespace std;

class Solution
{
public:
    int singNumber(vector<int> &nums)//寻找只出现一次的元素
    {

        unordered_map<int, int> hashTable; //创建表

        for (int num : nums)
        {   //等价于：for(vector<int>::iterator iter = nums.begin();
            //iter != nums.end(); ++iter)，作用是迭代容器中的所有的元素

            ++hashTable[num];
        }

        int ans = 0;

        for (auto [num, occ] : hashTable)
        {
            if (occ == 1)
            {//找到只出现一次的num
                ans = num;
                break;
            }
        }

        return ans;
    }
};

void test(){
    Solution p1;
    vector<int> a;
    int b[10] = {1, 2, 3, 1, 2, 3, 4, 3, 2, 1};
    for(int i = 0; i < 10; ++i){
        a.push_back(b[i]);//从数组中选择元素向向量中添加元素
    }
    int ret = p1.singNumber(a);
    cout << "SingleNumber = " << ret << endl;
}

int main(){
    test();
    return 0;
}

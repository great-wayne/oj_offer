/*
*  File    ValidateStackSequences.cc
*  Author  WayneGreat
*  Date    2021-09-12  16:52:22
*  Describe 
*/

#include <iostream>
#include <vector>
#include <stack>

using std::cout;
using std::endl;
using std::vector;
using std::stack;

class Solution {
public:
    bool validateStackSequences(vector<int> &pushed, vector<int> &popped)
    {
        //判断两个序列是否一致
        if (pushed.size() != popped.size()) {
            return false;
        }

        if (pushed.size() == 0 && popped.size() == 0) {//[] 与 [] 也可
            return true;
        }

        int len = pushed.size();
        int popIndex = 0;//出栈序列下标
        stack<int> st;//创建辅助栈

        for (int idx = 0; idx < len; ++idx) {
            st.push(pushed[idx]);
            while (popIndex < len && !st.empty() && st.top() == popped[popIndex]) {
                //出栈元素下标没有出界，栈不为空，栈顶元素为当前出栈序列下标的值
                st.pop();//出栈
                ++popIndex;//找下一个元素
            }
        }
        return st.empty();//当出栈序列可行时，最后的辅助栈为空，为true
    }
};
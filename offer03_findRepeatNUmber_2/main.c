#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.h"

int main() {
	int arr[] = { 2,3,5,4,1,2,6,7 };//1 - length-1
	// { 2,3,5,4,3,2,6,7 } 不能找到重复数字2，在1-2范围里有两个数(1/2)，2也是出现两次
	int res = getDuplication(arr, 8);
	printf("%d\n", res);
	system("pause");
	return 0;
}

//空间优先 不改变原数组 时间O(nlogn),空间O(1)
//此算法不能确定每个数字各出现一次还是某个数字出现两次
int getDuplication(const int* nums, int len) {
	if (nums == NULL || len < 1) {
		fprintf(stderr, "Error arr\n");
		return -1;
	}
	int start = 1;
	int end = len - 1;
	while (end >= start) {
		int mid = ((end - start) >> 1 )+ start;//mid用于二分
		int count = countRange(nums, len, start, mid);//二分查找思路
		if (end == start) {
			if (count > 1) {//找到重复的数
				return start;
			}
			else {//无重复的数
				break;
			}
		}
		if (count > (mid - start + 1)) {//此范围有重复的数
			end = mid;//缩小end->缩小mid
		}
		else {
			start = mid + 1;
		}
	}
	return -1;
}

int countRange(const int* nums, int len, int start, int end) {
	//按照二分查找的思路遍历数组计算数字在start-end范围内的数量
	if (nums == NULL) {
		return 0;//表示空数组
	}
	int count = 0;
	for (int i = 0; i < len; ++i) {
		if (nums[i] >= start && nums[i] <= end) {
			++count;
		}
	}
	return count;
}
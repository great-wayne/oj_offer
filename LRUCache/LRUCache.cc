/*
*  File    LRUCache.cc
*  Author  WayneGreat
*  Date    2021-09-09  14:52:30
*  Describe 
*/

/*
hash + list

就不自己写双链表了，用 c++ 自带的就行。

思路：

保持把新鲜数据往链表头移动。新鲜的定义：刚被修改(put)，或者访问过(get)，就算新鲜，就需要 splice 到链表头。
过期键直接 pop_back()，链表节点越往后，越陈旧。
代码要领：

map 中保存的是 <key, 链表节点的指针>，这样查找的时候就不用需要去遍历链表了，使用 unordered_map 就能很快找到链表节点指针。
判断容量的时候，最好不使用 std::list::size() 方法，在 c++ 里，这个方法可能不是 O(1) 的。
*/
#include <iostream>
#include <list>
#include <map>
#include <unordered_map>

using std::cout;
using std::endl;
using std::map;
using std::pair;
using std::list;
using std::unordered_map;

class LRUCache {
public:
    LRUCache(int capacity) 
    : _capacity(capacity)
    {}

    int get(int key)
    {
        auto it = _table.find(key);
        if (it != _table.end()) {
            _lru.splice(_lru.begin(), _lru, it->second);//it->second => 指向链表节点的指针
            //转移it所指向的元素到_lru，放在_lru.begin()之前
            return it->second->second;//value 
        }
        return -1;
    }

    void put(int key, int value)
    {
        auto it = _table.find(key);
        if (it != _table.end()) {
            _lru.splice(_lru.begin(), _lru, it->second);//插到链表头
            it->second->second = value;
        }
        else {
            if (_capacity == _table.size()) {
                // auto it = _lru.end();
                // _table.erase(it->first);

                _table.erase(_lru.back().first);

                _lru.pop_back();
            }
            _lru.emplace_front(key, value);//插入到链表头
            // _lru.push_front({key, value});
            //emplace_back 避免了 push_back 的额外复制或移动操作

            _table[key] = _lru.begin();//更新hashTable
            // _table.insert({key, _lru.begin()});
        }
    }

private:
    int _capacity;
    list<pair<int, int>> _lru;
    unordered_map<int, list<pair<int, int>>::iterator> _table;
    //map 中保存的是 <key, 链表节点的指针>，这样查找的时候就不用需要去遍历链表了，使用 unordered_map 就能很快找到链表节点指针。
};

//采用链表结构体
class LRUCache2
{
public:
    LRUCache2(int capacity)
        : _capacity(capacity)
    {
    }
    
	int get(int key) 
    //查找key，并返回value值
    {
        auto umit = _cache.find(key);
        if(umit == _cache.end())
        {
            return -1;
        }
        else
        //存在就更新链表中的该key的结点，将其放入链表头，表示最近访问过
        {
            _listNode.splice(_listNode.begin(), _listNode, umit->second);
            return umit->second->value;
        }
    }
    
    void put(int key, int value) 
    //插入key-value值
    {
        // unordered_map<int, list<CacheNode>::iterator>::iterator umit;
        auto umit = _cache.find(key);
        if(umit != _cache.end())
        //如果该key已经存在，更新value，并将其放在链表头，头部结点表示最近访问
        {
            umit->second->value = value;
            _listNode.splice(_listNode.begin(), _listNode, umit->second);
        }
        else
        {
            if(_capacity == _cache.size())
            //cache已满，删除链表尾结点，尾部表示上次被访问距离现在的时间最长
            {
                auto &delNode = _listNode.back();//找到尾结点CacheNode
                _cache.erase(delNode.key);//从uo_map中删除
                //uo_map的erase用法之一 => size_type erase( const key_type& key );

                _listNode.pop_back();//从list中删除
            }
            //在链表头插入，并插入到uo_map中
            _listNode.push_front(CacheNode(key, value));
            _cache.insert(std::make_pair(key, _listNode.begin()));
            // _cache[key] = _listNode.begin();
        }
    }

private:
    struct CacheNode 
    //链表结点，存放key-value
    {
        int key;
        int value;
        CacheNode(int k, int v)
            : key(k), value(v)
        {}
    };

private:
    int _capacity;
    list<CacheNode> _listNode;
    //节点放在list链表中
    unordered_map<int, list<CacheNode>::iterator> _cache;
    //储存key值以及在链表中的位置，哈希表，实现O(1)时间复杂度找到结点位置
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */
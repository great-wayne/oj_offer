/*
题目：请实现一个函数，输入一个整数，输出该数二进制表示中1的个数。
例如：把9表示成二进制是1001，有2位是1。因此如果输入9，该函数输出2。
*/

#include <iostream>

using namespace std;

class Solution {
public:
    int hammingWeight(uint32_t n) {

       int count = 0;

        /*把一个整数减去1，再和原整数做与运算，会把该整数的最右边的1变为0，
        那么一个整数的二进制表示有多少个1就会可以循环进行多少次这样的与操作，记录count
        */
       while(n)
       {
           ++count;
           n = (n - 1) & n;//（1）1100 - 1 -> 1011; （2）1011 & 1100 -> 1000
       }

       return count;
    }
};